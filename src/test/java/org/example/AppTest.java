package org.example;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AppTest
{
    private App app;
    String[] argc;

    public AppTest(String[] argc)
    {
        this.argc = argc;
        this.app = new App(argc);
    }

    @Parameterized.Parameters
    public static Collection primeNumbers()
    {
        return Arrays.asList(new Object[]{new String[]{"1"}}, new Object[]{new String[]{"10", "9", "8", "7", "6", "5", "4", "3", "2", "1", "4"}}, new Object[]{new String[]{}}, new Object[]{new String[]{"10", "9", "8", "7", "6", "5", "4", "3", "2", "1"}}  // Empty array as input
        );
    }

    @Test
    public void testPrimeNumberChecker()
    {

        int[] intArray = new int[argc.length];
        for (int i = 0; i < argc.length; i++)
        {
            intArray[i] = Integer.parseInt(argc[i]);
        }
        Arrays.sort(intArray);
        assertArrayEquals(app.getA(), intArray);

    }
}