package org.example;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

/**
 * Hello world!
 */
public class App
{

    private int[] a = new int[0];

    public int[] getA()
    {
        return a;
    }

    public static void main(String[] args)
    {
        App app = new App(args);
        app.printOutput();
    }

    public App(String[] argc)
    {
        if (argc.length == 0) return;
        a = new int[argc.length];
        for (int i = 0; i < argc.length; i++)
        {
            a[i] = Integer.parseInt(argc[i]);
        }
        Arrays.sort(a);
    }

    public void printOutput()
    {
        String strArr = a[0] + " ";
        for (int i = 1; i < a.length; i++)
        {
            strArr += Integer.toString(a[i]) + " ";
        }
        System.out.println(strArr.substring(0, strArr.length() - 2));

    }
}
